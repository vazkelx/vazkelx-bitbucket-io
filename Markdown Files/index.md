# PPSS - 2019

#### Adrián Vázquez Alarcón

### SESIONES

- [x] [Sesión 1](Sesiones/S01.html)
- [x] [Sesión 2](Sesiones/S02.html) 
- [x] [Sesión 3](Sesiones/S03.html) 
- [x] [Sesión 4](Sesiones/S04.html)
- [x] [Sesión 5](Sesiones/S05.html)
- [x] [Sesión 6](Sesiones/S06.html)
- [x] [Sesión 7](Sesiones/S07.html)
- [x] [Sesión 8](Sesiones/S08.html)
- [x] [Sesión 9](Sesiones/S09.html)
- [x] [Sesión 10](Sesiones/S10.html)
- [x] [Sesión 11](Sesiones/S11.html)
- [x] [Sesión 12](Sesiones/S12.html)

### PRÁCTICAS

- [x] [Práctica 1](Practicas/P01.html)
- [x] [Práctica 2](Practicas/P02.html)
- [x] [Práctica 3](Practicas/P03.html)
- [x] [Práctica 4](Practicas/P04.html)
- [x] [Práctica 5](Practicas/P05.html)
- [x] [Práctica 6](Practicas/P06.html) 
- [x] [Práctica 7a](Practicas/P07a.html) 
- [x] [Práctica 7b](Practicas/P07b.html) 
- [x] [Práctica 8](Practicas/P08.html) 
- [x] [Práctica 9](Practicas/P09.html) 
- [x] [Práctica 10](Practicas/P10.html) 
- [x] [Práctica 11](Practicas/P11.html) 

