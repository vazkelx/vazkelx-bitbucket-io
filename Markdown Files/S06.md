# S06 - DEPENDENCIAS EXPERNAS II

[Volver](javascript:history.back())

#### Frameworks para implementar dobles

​	Podemos utilizar algún **framework** para evitar tener que implementar de forma 'manual' los dobles. Con esto evitamos que el código adicional pueda añadir errores.

​	Generalmente, cualquier framework nos generará una implementación configurable de los dobles 'on the fly', generando el bytecode necesario. <u>Primero **configuraremos**</u> los dobles para controlar las entradas indirectas a nuestro SUT (stubs), o para registrar o verificar las salidas indirectas de nuestro SUT (spies y mocks.) después, **inyectaremos** los dobles en la unidad a probar antes de invocarla.

​	Los frameworks tienen defensores y detractores. La verificación basada en el **comportamiento** genera un riesgo de que los tests tengan un alto nivel de acoplamiento con los detalles de la implementación. Un framework contribuye a crear tests **potencialmente frágiles y difíciles de mantener**.

​	Algunos frameworks: **EasyMock**, Mockito, JMockit, PowerMock

#### Objetos Stub VS Objetos Mock

​	**<u>Test Stub</u>**: objeto que actúa como punto de control para entregar **ENTRADAS INDIRECTAS** al SUT. Utiliza verificación basada en el **ESTADO**.

​	**<u>Mock Object</u>**: objeto que actúa como punto de observación para las **SALIDAS INDIRECTAS** del SUT. Registra las llamadas recibidas del SUT, y compara las reales con las previamente definidas como expectativas. Utiliza verificación basada en el **COMPORTAMIENTO**.

![](img6.png)

#### Verificación basada en el ESTADO vs Verificación basada en el COMPORTAMIENTO

| **Driver con verificación basada en el <span style="color:red">ESTADO</span>** | **Driver con verificación basada en el <span style="color:red">COMPORTAMIENTO</span>** |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| **SETUP**: preparamos el objeto a probar como los stubs de los colaboradores. | **SETUP DATA**: preparamos el objeto que se va a probar. **SETUP EXPECTATIONS**: Programamos las expectativas de cada mock invocado desde nuestro SUT |
| **EXERCISE**: invocamos a la unidad a probar                 | **EXERCISE**: invocamos a la unidad a probar <br>**VERIFY EXPECTATIONS**: verificamos que se han invocado a los métodos correctos, en el orden correcto, con los parámetros correctos. |
| **VERIFY STATE**: utilizamos aserciones para comprobar el estado resultante de la invocación a nuestro SUT | **VERIFY STATE**: utilizamos aserciones para comprobar el estado resultante de la invocación a nuestro SUT |
| **TEARDOWN**: restauramos el estado si es necesario          | **TEARDOWN**: restauramos el estado si es necesario          |

#### EasyMock y tipos de dobles

| **<span style="color:red">NiceMock</span>**: para crear STUBS | **<span style="color:red">Mock</span>**: para crear MOCKS (si sólo hay 1 invocación al doble) | **<span style="color:red">StrictMock</span>**: para crear MOCKS |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| El **orden** de las invocaciones NO se chequean              | El **orden** de las invocaciones NO se chequean              | Se comprueba el **orden** en el que se realizan las llamadas el doble |
| Por defecto se permiten invocaciones a todos los métodos del objeto. Si no hemos programado las expectativas de algún método, éste devolverá valores 'vacíos' adecuados | Por defecto se lanza un **AssertionError** para cualquier invocación no deseada para todos los métodos del objeto | Si se invoca a un método no 'esperado' se lanza un **AssertionError** |
| Todas las llamadas esperadas a métodos realizadas por el doble deben realizarse con los argumentos especificados | Todas las llamadas esperadas a métodos realizadas por el doble deben realizarse con los argumentos especificados | Todas las llamadas esperadas a métodos realizadas por el doble deben realizarse con los argumentos especificados |

​	Debemos crear un doble **PARA CADA** dependencia externa!!

#### EasyMock: implementación de mocks

 1. **Creamos el mock**

    ```java
    import org.easymock.EasyMock;
    ...
        //si sólo invocamos a dep 1 vez desde nuestra SUT
        Dependencia1 dep1 = EasyMock.createMock(Dependencia1.class);
    	//en cualquier otro caso
        Dependencia2 dep2 = EasyMock.createStrictMock(Dependencia2.class);
    ```

	2. **Programamos las expectativas del mock**: determinamos el valor de las entradas y las salidas indirectas del SUT

    ```java
    //metodo1() será invocado desde nuestro SUT con los parámetros indicados y devolverá 9
    EasyMock.expect(dep2.metodo1("xxx",7)).andReturn(9);
    //metodo1() será invocado desde nuestro SUT y devolverá una excepción
    EasyMock.expect(dep2.metodo1("yy",4)).andThrow(new MyException("message"));
    //metodo2() será invocado desde nuestra SUT, es un método que devuelve void
    dep1.metodo2(15);
    ```

	3. **Indicamos al framework que el mock ya está listo para ser invocado por nuestro SUT**. SIEMPRE tendremos que ACTIVAR el mock

    ```java
    EasyMock.replay(dep1,dep2);
    ```

	4. **Verificamos las expectativas del mock**. SIEMPRE debemos verificar que efectivamente nuestra SUT ha invocado a los mocks.

    ```java
    EasyMock.verify(dep1,dep2);
    ```

#### EasyMock: programación de expectativas

​	Debemos indicar cómo interaccionará nuestro SUT con el objeto mock que hemos creado. Cuando ejecutemos nuestro SUT durante las pruebas, el mock registrará todas las interacciones desde el SUT:

* Si es **StrickMock** y las invocaciones del SUT no coinciden con las expectativas programadas (**nº invocaciones, parámetros y orden**), entonces el doble provocará un fallo.

* Si es **Mock** y las invocaciones del SUT no coinciden con las expectativas programadas (**Nº invocaciones y parámetros**), entonces el doble provocará un fallo.

  Para especificar las expectativas, podemos indicar:

* que se esperan un nº determinado de invocaciones

  ```java
  Expect(mock.metodoX("parametro")) 
      .andReturn(42).times(3)						//las 3 primeras veces -> 42
      .andThrow(new RuntimeException(), 4) 		//las 4 siguientes -> exception
      .andReturn(-42); 							//la última -> -42
  ```

* podemos relajar las expectativas:

  ```java
  times(int min, int max);	//número de invocaciones entre min y max
  atLeastOnce(); 				//se espera al menos una invocación
  anyTimes();					//nos da igual el número de invocaciones
  ```

* las expectativas pueden expresarse de forma encadenada

  ```java
  //Encadenada
  expect(mock.operation())
  	.andReturn(true).times(1,5)
  	.andThrow(new RuntimeException("message"));
  
  //Sin encadenar
  expect(mock.operation().andReturn(true).times(1,5);
  expectLastCall().andThrow(new RuntimeException("message"));
  ```

​	Si es un Mock y nuestro SUT realiza MÁS invocaciones de las programadas, esas invocaciones adicionales devolverán un resultado por defecto.

​	Con respecto a los <u>valores de los parámetros</u>:

* EasyMock, para comparar objetos, utiliza por defecto **equals()**, por lo que si no definimos este método no estaremos comprobando los valores de los atributos

* Si  queremos que el parámetro de la expectativa sea la misma instancia, usaremos el método **same()**

  ```java
  User user = new User();
  expect(userService.addUser(same(user))).andReturn(true);
  replay(userService);
  ```

* Los Arrays se comprueban por defecto con **Arrays.equals()**, por lo que se comprueban todos los valores del array

* También podemos relajar los valores de los parámetros

  ```java
  anyObject(); 	//indica que el argumento puede ser cualquier objeto
  anyBoolean();	//indica que el argumento puede ser cualquier booleano
  anyInt(); 		//indica que el argumento puede ser cualquier entero
  ...
  isNull(); 		//Comprueba que se trata de un valor nulo
  notNull(); 		//Comprueba que se trata de un valor no nulo
  ```

  Con respecto al <u>orden de ejecución</u> de las expectativas de **un mock**:

* Si usamos un Mock, el orden no se checkea

* Si usamos un StrickMock, el orden de las invocaciones debe coincidir exactamente con el orden establecido en las expectativas.

  Con respecto al <u>orden de ejecución</u> de las expectativas de **varios mocks**:

* Para poder establecer un orden de invocaciones entre objetos StrickMock, usaremos **IMocksControl**

```java
Doc1 mock1 = EasyMock.createStrictMock(Doc1.class);
Doc2 mock2 = EasyMock.createStrictMock(Doc2.class);
//si las expectativas determinan un cierto orden
//entre las invocaciones a mock1 y mock2,
//si nuestro SUT NO sigue ese orden de invocaciones
//el test NO falla
replay(mock1,mock2);
verify(mock1,mock2);

IMocksControl ctrl = EasyMock.createStrictControl();
Doc1 mock1 = ctrl.createMock(Doc1.class);
Doc2 mock2 = ctrl.createMock(Doc2.class);
//si las expectativas determinan un cierto orden
//entre las invocaciones a mock1 y mock2,
//si nuestro SUT no sigue ese orden de invocaciones
//el test fallará
ctrl.replay(); //no es necesario usar parámetros
ctrl.verify();
```

#### Implementación de un Driver

​	**Librería easyMock:**

```xml
<dependency>
    <groupId>org.easymock</groupId>
    <artifactId>easymock</artifactId>
    <version>4.0.2</version>
    <scope>test</scope>
</dependency>
```

```java
public class CurrencyUnitTest {
    
    @Test
    public void testToEuros() throws IOException {
        Currency testObject = new Currency(2.50, "USD");
        Currency expected = new Currency(3.75, "EUR"); //R. Esperado
        ExchangeRate mock = EasyMock.createMock(ExchangeRate.class); //#1 crear mock
        EasyMock.expect(mock.getRate("USD", "EUR")).andReturn(1.5);	//#2 llamada y return
        EasyMock.replay(mock); //#3 listos para ejecutar el mock
        Currency actual = testObject.toEuros(mock); //Ejecutamos el método
        EasyMock.verify(mock); //#4 verificamos que se ha llamado al mock desde el SUT
        assertEquals(expected, actual); //Comparamos el resultado
    }
}
```

#### EasyMock: implementación de Stubs

1. Creamos el stub

   ```java
   import org.easymock.EasyMock;
   ...
       Dependencia1 dep1 = EasyMock.createNiceMock(Dependencia1.class);
       //* dep1 no chequea el orden de invocaciones
       //* se permiten invocaciones no programadas, en ese caso se
       //  devolverán los valores por defecto 0, null o falso
   ```

2. programamos las expectativas del stub

   ```java
   //metodo1() devolverá 9 si es invocado por nuestro SUT
   //independientemente de cuándo o cuántas veces sea invocado
   //y con qué valores de parámetros sea invocado
   EasyMock.expect(dep1.metodo1(anyString(),anyInt()))
   	.andStubReturn(9);
   //metodo2() devolverá una excepción cuando se invoque desde SUT
   EasyMock.expect(dep1.metodo2(anyObject()))
   	.andStubThrow(new MyException("message"));
   dep1.metodo3(anyInt());
   EasyMock.expectLastCall.asStub();
   ```

   

3. Indicamos al framework que el stub ya está listo para ser invocado por el SUT

   ```java
   EasyMock.replay(dep1);
   ```

#### EasyMock: programación de expectativas (comportamiento de Stubs)

​	Un stub puede devolver resultados diferentes dependiendo de los valores de los parámetros de las invocaciones. También podemos relajar los valores de los parámetros de entrada del stub

```java
//creamos el doble
Dependencia dep = EasyMock.createNiceMock(Dependencia.class);
//programamos las expectativas
//CADA vez que nuestro SUT invoque a servicio4 con un 12, devolverá 25
//independientemente del número de invocaciones
EasyMock.expect(dep.servicio4(12)).andStubReturn(25);
//si nuestro SUT invoca a servicio4 con cualquier otro valor, devolverá 30
//independientemente del número de veces que se invoque
EasyMock.expect(dep.servicio4(EasyMock.not(EasyMock.eq(12)))).andStubReturn(30);
//si nuestro SUT invoca servicio5(8) siempre -> el stub devolverá null todas las veces
//null es el valor por defecto para los Strings
//si nuestra SUT no invoca nunca servicio5(3), el test NO fallará
EasyMock.expect(dep.servicio5(3)).andStubReturn("pepe");

//otros métodos que pueden usarse
and(X first, X second), or(X first, X second)//X puede ser de tipo primitivo o un objeto
lt(X value), leq(X value), geq(X value), gt(X value) //Para X = tipo primitivo
startsWith(String prefix), contains(String substring), endsWith(String suffix)
```

#### Partial mocking

​	Implementación ficticia de algunos métodos, cuando estamos probando un método que realiza llamadas a otros métodos de su misma clase. La librería EasyMock nos permite realizar un mocking parcial de una clase, utilizando el método **partialMockBuilder()**:

```java
ToMock mock = partialMockBuilder(ToMock.class)
	.addMockedMethod("mockedMethod").createMock();

Rectangle rec = partialMockBuilder(Rectangle.class)
	.addMockedMethods("convertX", "convertY")
	.createMock();
```

#### Resumen

| Verificación basada en el ESTADO                             | Verificación basada en el COMPORTAMIENTO                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| * Si no usamos un framework, puede que tengamos que implementar manualmente un número elevado de objetos stub<br>* Sin un framework, resulta más complejo el preparar todos los stubs, pero los tests son MAS ROBUSTOS ante cambios | * Usaremos siempre un framework que nos permita implementar rápidamente los mocks. Son más frágiles y difíciles de mantener. |

* Si nuestras dependencias externas no proporcionan entradas indirectas al SUT, usaremos un mock y tendremos que usar verificación basada en el comportamiento si queremos comprobar que el doble ha sido invocado
* Si los colaboradores proporcionan entradas indirectas al SUT, debemos controlar dichas entradas con un stub para realizar pruebas unitarias. Podemos usar o no un framework
* Si queremos verificar el comportamiento de nuestro SUT, necesariamente usaremos mocks.