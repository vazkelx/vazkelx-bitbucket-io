# P04 - CAJA NEGRA

[Volver](javascript:history.back())

### ENTRADAS

* <u>**E1**: Alumno.nif</u>
  * <span style="color:green">**E1V**: válido</span>
  * <span style="color:red">**E1NV1**: no válido</span>
  * <span style="color:red">**E1NV2**: null</span>
* <u>**E2**: List<AsignaturaTO\></u>
  * <span style="color:green">**E2V**: lista sin asignaturas repetidas</span>
  * <span style="color:red">**E2NV1**: null</span>
  * <span style="color:red">**E2NV2**: vacía</span>
  * <span style="color:red">**E2NV3**: lista.size() > 5</span>
  * <span style="color:red">**E2NV4**: asignaturas repetidas</span>
* <u>**E3**: Conexión db</u>
  * <span style="color:green">**E3V**: OK</span>
  * <span style="color:red">**E3NV1**: produce (Error al obtener los datos del alumno)</span>
  * <span style="color:red">**E3NV2**: produce (Error en el alta del alumno)</span>
  * <span style="color:red">**E3NV3**: produce (El alumno con nif nif_alumno ya está matriculado en la
    asignatura con código código_asignatura)</span>
  * <span style="color:red">**E3NV4**: produce (Error al matricular la
    asignatura cod_asignatura)</span>

### SALIDAS

* <span style="color:green">**SV**: MatriculaTO OK</span>
* <span style="color:red">**SNV1**: BOException(Error al obtener los datos del alumno)</span>
* <span style="color:red">**SNV2**: BOException(Error en el alta del alumno)</span>
* <span style="color:red">**SNV3**: BOException(El alumno con nif nif_alumno ya está matriculado en la
  asignatura con código código_asignatura)</span>
* <span style="color:red">**SNV4**: MatriculaTO List<errores\>(Error al matricular la
  asignatura cod_asignatura)</span>
* <span style="color:red">**SNV5**: BOException(El número máximo de asignaturas es cinco)</span>
* <span style="color:red">**SNV6**: BOException(El nif no
  puede ser nulo)</span>
* <span style="color:red">**SNV7**: BOException(Nif no válido)</span>
* <span style="color:red">**SNV8**: BOException(Faltan las asignaturas
  de matriculación)</span>

​	

|  ID  | Combinación                |
| :--: | -------------------------- |
|  1   | **E1V-E2V-E3V-SV**         |
|  2   | **E1NV1-**E2V-E3V-**SNV7** |
|  3   | **E1NV2**-E2V-E3V-**SNV6** |
|  4   | E1V-**E2NV1**-E3V-**SNV8** |
|  5   | E1V-**E2NV2**-E3V-**SNV8** |
|  6   | E1V-**E2NV3**-E3V-**SNV5** |
|  7   | E1V-**E2NV4**-E3V-**SNV4** |
|  8   | E1V-E2V-**E3NV1**-**SNV1** |
|  9   | E1V-E2V-**E3NV2**-**SNV2** |
|  10  | E1V-E2V-**E3NV3**-**SNV3** |
|  11  | E1V-E2V-**E3NV4**-**SNV4** |

| ID   | NIF       | List<asignaturas\>               | Estado DB                                                    | R. Esperado                                                  | R. Real |
| ---- | --------- | -------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------- |
| 1    | 74386536M | {ppss, mtis}                     | OK                                                           | MatriculaTO OK(74386536M, {ppss, mtis}, {})                  |         |
| 2    | 1111A     | {ppss, mtis}                     | OK                                                           | BOException(Nif no válido)                                   |         |
| 3    | (null)    | {ppss, mtis}                     | OK                                                           | BOException(El nif no puede ser nulo)                        |         |
| 4    | 74386536M | (null)                           | OK                                                           | BOException(Faltan las asignaturas de matriculación)         |         |
| 5    | 74386536M | {}                               | OK                                                           | BOException(Faltan las asignaturas de matriculación)         |         |
| 6    | 74386536M | {ppss, mtis, dca, gpi, dss, ada} | OK                                                           | El número máximo de asignaturas es cinco                     |         |
| 7    | 74386536M | {ppss, ppss}                     | OK                                                           | MatriculaTO List<errores\>(Error al matricular la asignatura cod_asignatura) |         |
| 8    | 74386536M | {ppss, mtis}                     | produce (Error al obtener los datos del alumno)              | BOException(Error al obtener los datos del alumno)           |         |
| 9    | 74386536M | {ppss, mtis}                     | produce (Error en el alta del alumno)                        | BOException(Error en el alta del alumno)                     |         |
| 10   | 74386536M | {ppss, mtis}                     | produce (El alumno con nif nif_alumno ya está matriculado en la asignatura con código código_asignatura) | BOException(El alumno con nif nif_alumno ya está matriculado en la asignatura con código código_asignatura) |         |
| 11   | 74386536M | {ppss, mtis}                     | produce (Error al matricular la asignatura cod_asignatura)   | MatriculaTO List<errores\>(Error al matricular la asignatura cod_asignatura) |         |

