# S05 - DEPENDENCIAS EXPERNAS I

[Volver](javascript:history.back())

#### Pruebas unitarias y dependencias externas

​	**Objetivo**: encontrar DEFECTOS en el código de las **UNIDADES** probadas.

​	Las pruebas de unidad dinámicas requieren **ejecutar cada unidad** de forma **AISLADA** para poder detectar defectos en dicha unidad. Necesitamos **CONTROLAR** la ejecución de dichas dependencias externas si queremos **AISLAR** el código a probar.

#### La regla 'de Oro' para realizar las pruebas

​	El código de la unidad a probar tiene que ser exactamente el mismo código que se utilizará en producción. No está permitido alterar circunstancialmente/temporalmente el código de SUT de ninguna forma.

#### Código testable y control de dependencias

​	**Código testable**: aquel que permite que un componente sea fácilmente probado de forma AISLADA.

​	**Dependencia externa**: otra unidad en nuestro sistema con la cual interactúa nuestro código a probar, y sobre la que no tenemos ningún control.

​	Para poder probar un componente de forma aislada debemos **CONTROLAR** sus DEPENDENCIAS externas , también denominadas COLABORADORES o DOCs(Depended-On Component). Para poder realizad este REEMPLAZO controlado necesitamos que SUT contenga uno o varios **SEAMS**.

#### Concepto de SEAM

​	*A seam is a place where you can alter behavior in your program **without editing in that place**. Every seam has an **enabling point**, a place where you can make the decision to use one behavior or another.*

​	Para poder conseguir un SEAM, PUEDE que necesitemos REFACTORIZAR nuestro SUT. Los **DOBLES** son reemplazos controlados de los controladores del sistema utilizados durante las pruebas para aislar el código de nuestro SUT.

​	Si **podemos cambiar el método** que se invocará **SIN** alterar el código de la unidad que lo contiene, entonces será un **SEAM**.

#### Pasos a seguir para automatizar las pruebas

1. Identificar las **dependencias** externas
2. Asegurarnos de que nuestro código es **TESTABLE**. Probablemente necesitaremos **REFACTORIZAR** nuestro SUT.
3. Proporcionamos un **DOBLE**
4. **Implementamos los DRIVERS**. Para ello podemos hacer verificación basada en el **ESTADO** y en el **COMPORTAMIENTO**:
   * <u>Verificación basada en el **ESTADO**</u>: interesados en comprobar el estado resultante de la invocación de nuestro SUT
   * <u>Verificación basada en el **COMPORTAMIENTO**</u>: nos interesa además, verificar que las interacciones entre nuestro SUT y las dependencias externas se realizan correctamente



​	Nuestro **DOBLE** debe IMPLEMENTAR la misma INTERFAZ que el colaborador(DOC) o debe EXTENDER la misma CLASE que el colaborador(DOC).

​	Nuestra SUT será TESTABLE si podemos 'inyectar' ducho doble en nuestra SUT durante las pruebas de alguna de las siguientes formas:

* como un <u>parámetro</u>
* a través del <u>constructor</u> de la clase
* a través de un <u>método setter</u>
* a través de un método de <u>factoría local</u> o una <u>clase factoría</u>

​	Si nuestra SUT NO es testable, entonces tendremos que **REFACTORIZAR** el código de nuestra SUT para que podamos inyectar el doble de alguna de las formas anteriores, teniendo en cuenta que:

* :one: Si añadimos un <u>parámetro</u>, estamos OBLIGANDO a que cualquier código cliente de nuestra SUT tenga que CONOCER dicha dependencia ANTES de invocar a nuestra SUT

* :two: - :three: Si añadimos un parámetro al <u>constructor</u> de nuestra SUT, estaremos OBLIGADOS a delarar la dependencia como atributo de la clase que contiene nuestro SUT.
* :three: No podremos añadir un <u>método setter</u> si el constructor realizase alguna acción significativa sobre nuestra dependencia.
* :four: Si usamos un método de <u>factoría local</u>, no se ven afectados, ni los clientes de nuestro SUT, ni la estructura de clase que contiene nuestro SUT, aunque alteramos el comportamiento de la clase que contiene nuestro SUT al añadir un método. Una <u>clase factoría</u> implica añadir código en src/main/java que puede ser innecesario en producción.

#### Implementación del doble

​	**Test Double**: cualquier objeto o componente que se utilice para sustituir al objeto o componente real, con el propósito de realizar las pruebas. (Dummy Object, **Test Stub**, Test Spy, **Mock Object**, Fake Object) Usos:

* Aislar el código a probar
* Acelerar la velocidad de ejecución
* Conseguir ejecuciones deterministas cuando el comportamiento depende de situaciones aleatorias o dependientes del tiempo
* Simular condiciones especiales
* Conseguir acceder a información oculta

​	**Test Stub**: objeto que actúa como un <u>punto de control</u> para entregar **ENTRADAS INDIRECTAS**. Un stub utiliza <u>verificación basada en el estado</u>.

​	**Mock Object**: objeto que actúa como un <u>punto de observación</u> para las **SALIDAS INDIRECTAS**. Puede devolver información cuando se le invoca o no. Además registra las llamadas recibidas del SUT, y compara las llamadas reales con las dobles, de forma que hacen que el test falle si no se cumplen dichas expectativas. Un mock utiliza <u>verificación basada en el comportamiento</u>.

![](img5.png)