# S07 - PRUEBAS DE INTEGRACIÓN

[Volver](javascript:history.back())

#### Niveles de las pruebas

* Nivel de unidades
* **Nivel de integración:** <u>Objetivo</u> encontrar defectos derivados de la INTEGRACIÓN entre las unidades previamente probadas
* Nivel de sistema

#### Importancia de las pruebas de integración

​	Hasta ahora hemos probado las 'piezas' del sistema por separado, la siguiente tarea es 'reunir' todas las 'piezas' para construir el sistema completo. Construir el sistema completo a partir de sus piezas no es tarea fácil debido a los numerosos errores sobre las INTERFACES.

​	Un **sistema** es una colección de componentes interconectados de una determinada forma para cumplir un determinado objetivo.

#### El proceso de integración

​	El objetivo de la integración del sistema es construir una versión 'operativa' del sistema mediante: la agregación de forma **incremental** de nuevos  componentes, asegurándonos de que la adición de nuevos no 'perturba' el  funcionamiento de los que ya existen (**pruebas de regresión**).

​	las pruebas de integración del sistema constituyen un proceso sistemático para 'ensamblar' un sistema software, durante el cual se ejecutan pruebas conducentes a descubrir errores asociados con las **interfaces** de dichos componentes.

​	Las <u>pruebas de REGRESIÓN</u> consisten en repetir las pruebas realizadas cuando integramos un nuevo componente.

#### Tipos de interfaces y errores comunes

* <u>Tipos de Interfaces</u>:
  * **Interfaz a través de parámetros: ** los datos pasan de un componente a otro en forma de parámetros. Los métodos de un objeto tienen esta interfaz.
  * **Memoria compartida: **se comparte un bloque de memoria entre los componentes. Los componentes escriben datos en la memoria compartida, que son leídos por otros.
  * **Interfaz procedural: **un componente encapsula un conjunto de procedimientos que pueden llamarse desde otros componentes. Los objetos tienen esta interfaz.
  * **Paso de mensajes: **un componente A prepara un mensaje y lo envía al componente B. El mensaje de respuesta del componente B incluye los resultados de la ejecución del servicio. Los servicios web tienen esta interfaz.
* <u>Errores más comunes</u>:
  * Mal uso de la interfaz
  * Malentendido sobre la interfaz
  * Errores temporales

​	Las pruebas para detectar defectos en las interfaces son difíciles ya que algunos de ellos pueden sólo manifestarse bajo condiciones inusuales.

#### Guías generales para diseñar las pruebas

* Listar de forma explícita cada llamada a un componente externo. Diseñar un conjunto de pruebas con los valores de los parámetros a componentes externos en los **extremos de los rangos**. Estos valores pueden revelar inconsistencias de la interfaz con una mayor probabilidad.

* Si se pasan punteros, siempre probar con punteros nulos.

* Cuando se invoca a un componente con una **interfaz procedural**, diseñar tests que provoquen que el comportamiento falle.

* Utilizar pruebas de estrés en sistemas de **paso de mensajes**.

* Cuando varios componentes interaccionan con **memoria compartida**, diseñaremos los tests en el orden en los que los componentes con activados.

  Usaremos algún método de diseño de caja negra(p. ej. particiones equivalentes)

#### Estrategias de integración

* **Big Bang: ** una vez realizadas las pruebas unitarias, se integran todas las unidades
* **Top-down: ** integramos primero los componentes con mayor nivel de abstracción y vamos añadiendo componentes cada vez con menor nivel de abstracción.
* **Bottom-up: **integramos primero los componentes de infraestructura que proporcionan servicios comunes( acceso a bbdd, acceso a red, etc) y posteriormente añadimos componentes funcionales, cada vez con mayor nivel de abstracción
* **Sandwich: ** es una mezcla de las dos estrategias anteriores
* **Dirigida por los riesgos: ** se eligen primero componentes que tengan mayor riesgo
* **Dirigida por las funcionalidades: ** se ordenan las funcionalidades con algún criterio y se integra de acuerdo con este orden

#### DbUnit

​	Framework de código abierto basado en JUnit. Proporciona una solución 'elegante' para controlar la dependencia de aplicaciones con una bbdd. Permite gestionar el estado de una bbdd durante las pruebas y permite ser utilizado juntamente con JUnit. Escenario típico:

1. Eliminar cualquier estado previo de la DB resultante de pruebas anteriores
2. Cargar los datos necesarios para las pruebas de la BD
3. Ejecutar las pruebas utilizando métodos de la librería DbUnit para las aserciones

​	DBunit **NO** sustituye a la base de datos, solo nos permite **CONTROLAR** su estado previo y verificar si estado posterior de la invocación a nuestro SUT. Componentes principales del API:

* IDataBaseTester --> JdbcDatabaseTester
* IDatabaseConnection
* DatabaseOperation
* IDataSet --> FlatXmlDataSet
*  ITable
* Assertion

#### Interfaz IDataBaseTester

​	Interfaz que permite el acceso a la BD, devuelve conexiones con una BD de tipo IDatabaseConection. La implementación <u>JdbcDatabaseTester</u> usa <u>DriverManager</u> para obtener conexiones con la BD. Métodos:

* **onSetUp()**: realiza una operación CLEAN_INSERT con los datos indicados por <u>setDataSet()</u>. Debe invocarse desde un método anotado con <u>@BeforeEach</u>. Podemos cambiar la operación con <u>setSetUpOperation(DatabaseOperation operation)</u>
* **setDataSet(IDataSet dataSet), getDataSet()**: determina o recupera los datos  de la BD
* **getConnection()**: devuelve la conexión con la BD(IDataBaseConection)

```java
public class TestDBUnit {

    private IDatabaseTester databaseTester;
    
    @BeforeEach
    public void setUp() throws Exception {
    
    	databaseTester = new 																		JdbcDatabaseTester("jdbc:mysql://localhost:3306/DBUNIT","root", "");
 		...
   		databaseTester.setDataSet(dataSet);
        
    	databaseTester.onSetup();
        }
    
        @Test
        public void testInsert() throws Exception {       
        IDatabaseConnection connection = databaseTester.getConnection();
        ...
        }
}
```

**Clase DatabaseOperation** : Clase abstracta que define el contrato de la interfaz para OPERACIONES realizadas sobre la DB. Utilizaremos un <u>dataset</u> como entrada:

* <u>DatabaseOperation.DELETE_ALL</u>: Elimina todas las filas de las tablas especificadas en el dataset
* <u>DatabaseOperation.CLEAN_INSERT</u>: Realiza una operación DELETE_ALL, seguida de un INSERT utilizada en el método onSetUp()
* <u>DatabaseOperation.REFRESH</u>: actualiza e inserta datos basados en el dataset. Los datos existentes no se ven afectados

**Interfaz IDatabaseConnection**: Representa una CONEXIÓN con una BD. Métodos:

* <u>createDataSet()</u>: crea dataset con TODOS los datos existentes actuales en la BD
* <u>createDataSet(tablas)</u>: crea un dataset de las tablas indicadas
* <u>createTable(tabla)</u>: crea objeto ITable con el resultado de la query (select * from tabla) sobre la base de datos
* <u>createQueryTable(tabla, sql)</u>: crea objeto ITable con el resultado de la query
* <u>getConfig()</u>: devuelve objeto DatabaseConfig configuración de la conexión
* <u>getRowCount(tabla)</u>: devuelve el nº de filas de la tabla

| Interfaz ITable                                              | Interfaz IDataSet                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Representa una colección de datos tabulares. Usado en aserciones, para comparar tablas. Implementaciones:<br>* <u>DefaultTable</u>: ordenación por clave primaria<br>* <u>SortedTable</u>: vista ordenada de la tabla<br>* <u>ColumnFilterTable</u>: filtra columnas de la tabla | Representa una colección de tablas. Usado para situal la BD en un estado determinado, y para comparar el estado. Implementaciones:<br>* <u>FlatXmlDataSet</u>: lee/escribe en xml<br>* <u>QueryDataSet</u>: guarda colecciones de datos resultantes de una query<br>Métodos:<br>* <u>getTable(tabla)</u>: devuelve la tabla específica |

#### Clase FlatXmlDataSet

```xml
<!-- Ejemplo -->
<?xml version="1.0" encoding="UTF-8"?>
<dataset>
	<customer id="1" firstname="John" street="1 Main Street" />
	<user id="1" login="John" password="John" />
	<user id="2" login="Karl" password="Karl" />
</dataset>
```

#### Clase Assertion

​	Define métodos estáticos para realizar aserciones. <u>Métodos</u>:

* <u>assertEquals(IDataSet, IDataSet)</u>
* <u>assertEquals(ITable, ITable)</u>

```java
/*
	Ejemplos de IDatabaseConnection, ITable, IDataSet, Assertion
*/
public class TestDBUnit {
    
	@Test
	public void testInsert() throws Exception {
	...
	// obtenemos la conexión con la BD
	IDatabaseConnection connection = databaseTester.getConnection();
	
	//recuperamos TODOS los datos de la BD y los guardamos en un IDataSet
	IDataSet databaseDataSet= connection.createDataSet();
        
	//recuperamos los datos de la tabla "customer"
	ITable actualTable = databaseDataSet.getTable("customer");
        
	//establecemos los valores esperados desde el fichero customer-expected.xml
	DataFileLoader loader = new FlatXmlDataFileLoader();
	IDataSet expectedDataSet = loader.load("/customer-expected.xml");
	ITable expectedTable = expectedDataSet.getTable("customer");

    //comparamos la tabla esperada con la real
    Assertion.assertEquals(expectedTable, actualTable);
}
```

#### DbUnit y pruebas de integración con Maven

###### 	pom.xml

```xml
<!-- Librería DbUnit -->
<dependency>
    <groupId>org.dbunit</groupId>
    <artifactId>dbunit</artifactId>
    <version>2.6.0</version>
    <scope>test</scope>
</dependency>
<!-- Librería para el conector mysql -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.47</version>
    <scope>test</scope>
</dependency>
```

```xml
<!-- Plugin failsafe para ejecutar los tests de integración -->
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-failsafe-plugin</artifactId>
            <version>2.22.1</version>
            <executions>
            	<execution>
                    <goals>
                    <goal>integration-test</goal>
                    <goal>verify</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

#### Plugin SQL-Maven-Plugin (Script inicialización BD)

###### 	pom.xml

```xml
<!-- Plugin para ejecutar sentencias SQL -->
<plugin>
    <groupId>org.codehaus.mojo</groupId>
    <artifactId>sql-maven-plugin</artifactId>
    <version>1.5</version>
    <dependencies>
        <!-- Dependencia con el driver JDBC para acceder a una base de datos MySQL -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.47</version>
        </dependency>
    </dependencies>
    <!-- Configuración del driver JDBC -->
    <configuration>
        <driver>com.mysql.jdbc.Driver</driver>
        <url>jdbc:mysql://localhost:3306?useSSL=false</url>
        <username>root</username>
        <password>ppss</password>
    </configuration>
    <executions>
        <execution>
        <id>create-customer-table</id>
        <phase>pre-integration-test</phase> <!-- ejecutar antes ejecutar los test IT -->
        <goals>
        	<goal>execute</goal>
        </goals>
        customer
        <configuration>
            <srcFiles>
            	<srcFile>src/test/resources/sql/create_table_customer.sql</srcFile>
            </srcFiles>
        </configuration>
        </execution>
    </executions>
</plugin>
```

#### Proceso completo para automatizar las pruebas de integración con maven

* **pre-integration-test**: se ejecutan acciones previas a la ejecución de los tests
* **integration-test**: se ejecutan los tests de integración. Si un test falla, no se detiene la ejecución
* **post-integration-test**: detendremos los servicios o realizaremos las operaciones necesarias para restaurar el entorno de pruebas
* **verify**: se comprueba que todo está listo. Si un test ha fallado, se detiene la construcción

![](img7.png)

