# P11 - ANÁLISIS DE PRUEBAS: COBERTURA

[Volver](javascript:history.back())

## RESUMEN

...

​	

## EJERCICIOS

#### Ej. 1

##### A)

CC = nº condiciones +1 = **3**

El nº de casos de prueba ha sido 2. Porque cc es el máximo requerido de nº de casos de prueba, no es necesario llegar a ese número, pero no podemos pasarnos.



```java
public class MultipathExample {

    public int multiPath(int a, int b, int c) {
        if (a > 5) {
            c += a;
        }
        if (b > 5) {
            c += b;
        }
        return c;
    }
}


class MultipathExampleTest {
    int a, b, c;

    int real, esperado;

    MultipathExample  sut = new MultipathExample();
    
    @Test
    public void multiPath_C1(){
        a = 10;
        b = 10;
        c = 0;

        real = sut.multiPath(a, b, c);
        esperado = 20;

        Assertions.assertEquals(esperado, real);
    }

    @Test
    public void multiPath_C2(){
        a = 0;
        b = 0;
        c = 0;

        real = sut.multiPath(a, b, c);
        esperado = 0;

        Assertions.assertEquals(esperado, real);
    }
}



```

​	
